
from pypelinify.cli import main


def test_main():
    main([])


def test_library():
    from pypelinify import cache_point, Pypeline

    def add_one(x):
        return x+1

    add_one_wrapped = cache_point(add_one)

    def add_two(x):
        return add_one_wrapped(x) + 1

    pipeline = Pypeline(add_two)
    result = pipeline.run(5)
    stage_0 = pipeline.stages[0]

    assert result == 7
    assert stage_0.function == add_one
    assert stage_0.args == (5,)
