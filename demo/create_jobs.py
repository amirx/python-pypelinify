from time import sleep
from pypelinify import Pypelinifier
from pypelinify import cache_point

@cache_point
def slow_add_five(x: int):
    sleep(5)
    return x + 5


def slow_add_ten(x: int):
    return slow_add_five(slow_add_five(x))


pypelinifier = Pypelinifier()

pypelinifier.add_pipeline(slow_add_five)
pypelinifier.add_pipeline(slow_add_ten)


if __name__ == "__main__":
    pypelinifier.command_line()
