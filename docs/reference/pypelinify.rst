pypelinify
==========

.. testsetup::

    from pypelinify import *

.. automodule:: pypelinify
    :members:
