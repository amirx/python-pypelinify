========
Overview
========

.. start-badges

.. list-table::
    :stub-columns: 1

    * - docs
      - |docs|
    * - tests
      - | |travis| |appveyor| |requires|
        |
    * - package
      - | |version| |wheel| |supported-versions| |supported-implementations|
        | |commits-since|
.. |docs| image:: https://readthedocs.org/projects/python-pypelinify/badge/?style=flat
    :target: https://readthedocs.org/projects/python-pypelinify
    :alt: Documentation Status

.. |travis| image:: https://travis-ci.org/bakhtiary/python-pypelinify.svg?branch=master
    :alt: Travis-CI Build Status
    :target: https://travis-ci.org/bakhtiary/python-pypelinify

.. |appveyor| image:: https://ci.appveyor.com/api/projects/status/github/bakhtiary/python-pypelinify?branch=master&svg=true
    :alt: AppVeyor Build Status
    :target: https://ci.appveyor.com/project/bakhtiary/python-pypelinify

.. |requires| image:: https://requires.io/github/bakhtiary/python-pypelinify/requirements.svg?branch=master
    :alt: Requirements Status
    :target: https://requires.io/github/bakhtiary/python-pypelinify/requirements/?branch=master

.. |version| image:: https://img.shields.io/pypi/v/pypelinify.svg
    :alt: PyPI Package latest release
    :target: https://pypi.org/project/pypelinify

.. |commits-since| image:: https://img.shields.io/github/commits-since/bakhtiary/python-pypelinify/v0.0.0.svg
    :alt: Commits since latest release
    :target: https://github.com/bakhtiary/python-pypelinify/compare/v0.0.0...master

.. |wheel| image:: https://img.shields.io/pypi/wheel/pypelinify.svg
    :alt: PyPI Wheel
    :target: https://pypi.org/project/pypelinify

.. |supported-versions| image:: https://img.shields.io/pypi/pyversions/pypelinify.svg
    :alt: Supported versions
    :target: https://pypi.org/project/pypelinify

.. |supported-implementations| image:: https://img.shields.io/pypi/implementation/pypelinify.svg
    :alt: Supported implementations
    :target: https://pypi.org/project/pypelinify


.. end-badges

Seemlessly split your project into filters and pipelines

* Free software: BSD 2-Clause License

Installation
============

::

    pip install pypelinify

Documentation
=============


https://python-pypelinify.readthedocs.io/


Development
===========

To run the all tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
