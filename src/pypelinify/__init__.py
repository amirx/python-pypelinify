import argparse

from pypelinify.cli.commands import listing
from pypelinify.cli.parser import make_parser

__version__ = '0.0.0'


class Pypelinifier:
    def __init__(self):
        self.pypelines = []
        pass

    def add_pipeline(self, pipeline_start):
        self.pypelines.append(Pypeline(pipeline_start))

    def command_line(self):
        parser = make_parser()

        args = parser.parse_args()
        if vars(args) == {}:
            parser.print_help()
            exit(1)
        else:
            args.func(args, self.pypelines)


class Stage:
    def __init__(self, function, args, kwargs):
        self.function = function
        self.args = args
        self.kwargs = kwargs


_current_pipeline = None


def cache_point(function):
    global _current_pipeline

    def function_wrapper(*args, **kwargs):
        _current_pipeline.add_stage(function, *args, **kwargs)
        return function(*args, **kwargs)

    return function_wrapper


class Pypeline:
    def __init__(self, entry):
        self.stages = []
        self.entry = entry
        self.name = entry.__name__

    def run(self, *args, **kwargs):
        with PipelineContext(self):
            return self.entry(*args, **kwargs)

    def add_stage(self, function, *args, **kwargs):
        self.stages.append(Stage(function, args, kwargs))


class PipelineContext:

    def __init__(self, pipeline: Pypeline):
        global _current_pipeline
        assert _current_pipeline is None
        _current_pipeline = pipeline

    def __enter__(self):
        pass

    def __exit__(self, *args):
        global _current_pipeline
        _current_pipeline = None
