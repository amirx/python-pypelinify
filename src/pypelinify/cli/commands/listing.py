from pypelinify.service.listing import Listing


def call(args, pipelines):
    print(f"listing pipelines")
    service = Listing()
    service.list(pipelines)


def define_parser(parser):
    parser.set_defaults(func=call)
