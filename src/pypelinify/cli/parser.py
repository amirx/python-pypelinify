import argparse

from pypelinify.cli.commands import listing


def make_parser():
    parser = argparse.ArgumentParser(description='Train and deploy machine learning estimators')
    subparsers = parser.add_subparsers()
    listing.define_parser(subparsers.add_parser('list'))
    return parser


